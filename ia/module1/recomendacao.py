from math import sqrt

avaliacoes = {'Ana':
              {'Freddy x Jason': 2.5,
               'O Ultimato Bourne': 3.5,
               'Star Trek': 3.0,
               'Exterminador do Futuro': 3.5,
               'Norbit': 2.5,
               'Star Wars': 3.0},

              'Marcos':
              {'Freddy x Jason': 3.0,
               'O Ultimato Bourne': 3.5,
               'Star Trek': 1.5,
               'Exterminador do Futuro': 5.0,
               'Star Wars': 3.0,
               'Norbit': 3.5},

              'Pedro':
              {'Freddy x Jason': 2.5,
               'O Ultimato Bourne': 3.0,
               'Exterminador do Futuro': 3.5,
               'Star Wars': 4.0},

              'Claudia':
              {'O Ultimato Bourne': 3.5,
               'Star Trek': 3.0,
               'Star Wars': 4.5,
               'Exterminador do Futuro': 4.0,
               'Norbit': 2.5},

              'Adriano':
              {'Freddy x Jason': 3.0,
               'O Ultimato Bourne': 4.0,
               'Star Trek': 2.0,
               'Exterminador do Futuro': 3.0,
               'Star Wars': 3.0,
               'Norbit': 2.0},

              'Janaina':
              {'Freddy x Jason': 3.0,
                  'O Ultimato Bourne': 4.0,
               'Star Wars': 3.0,
               'Exterminador do Futuro': 5.0,
               'Norbit': 3.5},

              'Leonardo':
              {'O Ultimato Bourne': 4.5,
                  'Norbit': 1.0,
                  'Exterminador do Futuro': 4.0}
              }


avaliacao = avaliacoes

#print(avaliacao['Ana']['Star Trek'])
#print(avaliacao['Marcos']['Star Trek'])
# print(avaliacao['Ana'])

# Calculando a distância euclidiana entre pontos


calculoDecimal = sqrt(pow(3-1.5, 2) + pow(3.5 - 4, 2))
print(calculoDecimal)

# é colocado o + 1 no denominador, para não dar zero
calculoPorcentagem = 1 / (1 + sqrt(pow(3-1.5, 2) + pow(3.5 - 4, 2)))
print(calculoPorcentagem)
