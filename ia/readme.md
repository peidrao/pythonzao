# Inteligência Artificial & Sistema de Recomendação

## Conteúdo

1. Introdução aos Sistemas de Recomendação
2. Filtragem Colaborativa
3. Instalaão das Ferramentas
4. Busca por usuário similares
5. Recomendação de filmes(filtragem baseada em usuário)
6. Recomendação de filmes (filtragem baseada em itens)
7. Base de dados MovieLens

### Módulo 1 - Busca por usuários similares (conteúdo)

1. Gráfico dos usuários e filmes
2. Distância Euclidiana (DE)
3. DE para retornar os usuários mais similares
