from datetime import datetime
import xlrd

planilha = xlrd.open_workbook('planilha.xlsx')

# Onde está localizada cada tabela dentro da planilha
cell = planilha.sheet_by_index(1)
print(cell.nrows)

# cell.name -> nome da tabela que está sendo trabalhada
# cell.nrows -> número de linhas da tabela
# cell.ncols -> número de colunas.
print("{0} {1} {2}".format(cell.name, cell.nrows, cell.ncols))

for row in range(cell.nrows):
    for col in range(cell.ncols):
        if col == 0 and row != 0:
            # format date
            raw_value = cell.cell_value(row, col)
            converted_date = xlrd.xldate_as_tuple(
                raw_value, planilha.datemode)

            to_print_date = datetime(*converted_date).strftime('%d/%m/%y')

            print(to_print_date)
        else:
            print(cell.cell_value(row, col), end='')
        print('\t', end='')
    print()
