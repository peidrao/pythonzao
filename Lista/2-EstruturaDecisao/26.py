def menu():
    print('                           Até 5 Kg           Acima de 5 Kg\n'
          '(F) - File Duplo:    R$ 4,90 por Kg          R$ 5,80 por Kg\n'
          '(A) - Alcatra:       R$ 5,90 por Kg          R$ 6,80 por Kg\n'
          '(P) - Picanha:       R$ 6,90 por Kg          R$ 7,80 por Kg\n')


def cartao():
    aux = input('Possui o cartão Tabajara? (S) - Sim | (N) - Não: ')
    if(aux == 'S'):
        return True
    return False


def carnes(opc, kg):
    total = float
    if(opc == 'F'):
        if(kg < 5):
            total = kg * 4.9
        else:
            total = kg * 5.8
    if(opc == 'A'):
        if(kg < 5):
            total = kg * 5.9
        else:
            total = kg * 6.8
    if(opc == 'P'):
        if(kg < 5):
            total = kg * 6.9
        else:
            total = kg * 7.8

    if(cartao()):
        desconto = total - (total * 0.05)
        print('Valor + Desconto: R$', desconto)
    else:
        print('Valor: R$', total)


def main():
    menu()
    opcao = input('Escolha um tipo de carne: ')
    quilos = float(input('Quantos quilos de carne deseja: '))
    carnes(opcao, quilos)


main()
