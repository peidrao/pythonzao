def calcularGasto(litros, codigo):
    if(codigo == 'A'):
        if(litros < 20):
            return ((litros * 1.9) - ((litros * 1.9) * 0.03))
        else:
            return ((litros * 1.9) - ((litros * 1.9) * 0.05))
    elif(codigo == 'G'):
        if(litros < 20):
            return ((litros * 2.5) - ((litros * 2.5) * 0.04))
        else:
            return ((litros * 2.5) - ((litros * 2.5) * 0.06))


def main():
    cod = input('Qual tipo de gasolina: (A)-Alcool | (G)-Gasolina: ')
    l = int(input('Quantos litros: '))

    print('Valor Total: R$', calcularGasto(l, cod))


main()
