def equacao(a, b, c):
    delta = ((pow(b, 2)) - 4 * (a*c))
    if(delta < 0):
        print('Equação não possui raizes reais!')
        return
    elif(delta == 0):
        print('Única raiz: ', (-b/(2*a)))
    elif(delta > 0):
        print('X1: ', (-b - delta / (2*a)))
        print('X2: ', (-b + delta / (2*a)))


def main():
    a = int(input('Digite o valor de A: '))
    if(a > 0):
        b = int(input('Digite o valor de B: '))
        c = int(input('Digite o valor de C: '))
        equacao(a, b, c)
    else:
        print('O valor de A não pode ser 0')


main()
