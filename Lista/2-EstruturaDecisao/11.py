def calculo(salario):
    if(salario < 280):
        percentual = 20
        aumento = salario * (percentual/100)
        total = salario + aumento
    elif(salario >= 280 and salario < 700):
        percentual = 15
        aumento = salario * (percentual/100)
        total = salario + aumento
    elif(salario >= 700 and salario < 1500):
        percentual = 10
        aumento = salario * (percentual/100)
        total = salario + aumento
    else:
        percentual = 5
        aumento = salario * (percentual/100)
        total = salario + aumento

    print('Salário antes:  R$', salario, '\nPercentual: ', percentual,
          '%\nValor do aumento: R$', aumento, '\nNovo salário: R$', total)


def main():
    salario = float(input('Digite seu salário: '))
    calculo(salario)


main()
