def main():
    numero = float(input('Digite um número: '))
    print('Inteiro') if numero == round(numero) else print(
        'Numéro flutuante!\nPara cima:', round(numero+0.5), '\nPara baixo: ', round(numero-0.5))


main()
