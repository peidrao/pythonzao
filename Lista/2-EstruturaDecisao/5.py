def media(a, b):
    return (a+b)/2


def main():
    nota1 = float(input('Primeira nota: '))
    nota2 = float(input('Segunda nota nota: '))

    nota = media(nota1, nota2)

    if(nota == 10):
        print('Aprovado com distinção!')
    elif(nota >= 7):
        print('Aprovado!')
    else:
        print('Reprovado!')


main()
