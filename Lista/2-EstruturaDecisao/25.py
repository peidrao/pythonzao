def fruteira(maca, morango, contador):
    totalMorango = float
    totalMacas = float
    total = float
    if(maca < 5):
        totalMacas = maca * 1.8
    else:
        totalMacas = maca * 1.5
    if(morango < 5):
        totalMorango = morango * 2.5
    else:
        totalMorango = morango * 2.2

    total = totalMorango + totalMacas
    if (contador > 8 or total > 25):
        print('Valor total + desconto 10%: R$', total - (total * 0.1))
    else:
        print('Valor total: R$', total)


def main():
    maca = float(input('Quantos quilos de maçã deseja: '))
    mora = float(input('Quantos quilos de morango deseja: '))
    acumulado = int(mora + maca)
    fruteira(maca, mora, acumulado)


main()
