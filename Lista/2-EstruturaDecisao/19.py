def caixa(valor):
    cem = int(valor / 100)
    if cem > 0:
        valor = valor - (cem*100)
        print('Notas R$100,00 = ', cem)
    cinquenta = int(valor/50)
    if cinquenta > 0:
        valor = valor - (cinquenta*50)
        print('Notas R$ 50,00 = ', cinquenta)
    dez = int(valor/10)
    if(dez > 0):
        valor = valor - (dez*10)
        print('Notas R$ 10,00 = ', dez)
    cinco = int(valor/5)
    if cinco > 0:
        valor = valor - (cinco*5)
        print('Notas R$  5,00 = ', cinco)
    um = valor
    if(um > 0):
        print('Notas R$  1,00 = ', um)


def main():
    saque = int(input('Valor do saque: '))
    if(saque >= 10 and saque < 600):
        caixa(saque)
    else:
        print('\nValor Mínimo: R$10\nValor Máximo: R$ 600')


main()
