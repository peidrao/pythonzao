def verificarTriangulo(ladoA, ladoB, ladoC):
    if(ladoA + ladoB > ladoC and ladoA + ladoC > ladoB and ladoB + ladoC > ladoA):
        if(ladoA == ladoB and ladoA == ladoC and ladoB == ladoC):
            print('Triângulo Equilátero!')
        elif(ladoA == ladoB or ladoA == ladoC or ladoB == ladoC):
            print('Triângulo Isósceles!')
        elif(ladoA != ladoB and ladoA != ladoC and ladoB != ladoC):
            print('Triângulo Escaleno!')
    else:
        print('Não é um triângulo!')


def main():
    a = int(input('Lado A: '))
    b = int(input('Lado B: '))
    c = int(input('Lado C: '))

    verificarTriangulo(a, b, c)


main()
