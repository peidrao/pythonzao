def calculadora(a, b, op):
    total = float
    if(op == '+'):
        total = a + b
    elif(op == '-'):
        total = a - b
    elif(op == '/'):
        total = a / b
    elif(op == '*'):
        total = a * b

    print('Total: ', total)
    print('Par') if(total % 2 == 0) else print('Ímpar')
    print('Positivo') if(total > 0) else print('Negativo')
    print('Inteiro') if(total == round(total)) else print('Decimal')


def main():
    numero1 = float(input('Digite um número: '))
    numero2 = float(input('Digite outro número: '))
    opecacao = input(
        'Qual operação deseja realizar? (+) - Soma | (-) - Subtração | (/) - Divisão | (*) - Multiplicação: ')
    calculadora(numero1, numero2, opecacao)


main()
