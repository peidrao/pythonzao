def status(notaA, notaB):
    media = (notaA + notaB) / 2
    passou = bool
    conceito = str

    if(media >= 9 and media == 10):
        conceito = 'A'
        print(conceito)
        passou = True
    elif(media >= 7.5 and media < 9):
        conceito = 'B'
        print(conceito)
        passou = True
    elif(media >= 6 and media < 7.5):
        conceito = 'C'
        print('C')
        passou = True
    elif(media >= 4 and media < 6):
        conceito = 'D'
        print(conceito)
        passou = False
    elif(media >= 0 and media < 4):
        conceito = 'E'
        print(conceito)
        passou = False

    if(passou):
        print('Nota 1:', notaA, ' | Nota 2: ', notaB,
              ' | Média: ', media, '\nAPROVADO')
    else:
        print('Nota 1:', notaA, ' | Nota 2: ', notaB,
              ' | Média: ', media, '\nREPROVADO')


def main():
    a = float(input('Primeira nota: '))
    b = float(input('Segunda nota: '))
    status(a, b)


main()
