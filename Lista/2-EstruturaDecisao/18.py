def verificar(dia, mes, ano):
    flag = False
    if(mes == 1 or mes == 3 or mes == 5 or mes == 7 or mes == 8 or mes == 10 or mes == 12):
        if(dia <= 31):
            flag = True
    elif(mes == 4 or mes == 6 or mes == 9 or mes == 11):
        if(dia <= 30):
            flag = True
    elif mes == 2:
        if (ano % 4 == 0 and ano % 100 != 0) or (ano % 400 == 0):
            if(dia <= 29):
                flag = True
        elif(dia <= 28):
            flag = True

    if(flag == True):
        print('Data válida!')
    else:
        print('Data inválida!')


def main():
    dd = int(input('Digite um dia: '))
    mm = int(input('Digite um mês (01,...,12): '))
    aaaa = int(input('Digite um ano: '))

    verificar(dd, mm, aaaa)


main()
