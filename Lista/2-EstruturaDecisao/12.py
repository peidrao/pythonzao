def calculo(valor, horas):
    salario = valor * horas
    pagaImposto = bool
    irPercentual = int
    ir = float

    if(salario < 900):
        pagaImposto = False
    elif(salario >= 900 and salario < 1500):
        ir = salario * 0.05
        irPercentual = 5
        pagaImposto = True
    elif(salario >= 1500 and salario < 2500):
        ir = salario * 0.1
        pagaImposto = True
    elif(salario >= 2500):
        ir = salario * 0.2
        pagaImposto = True

    if(pagaImposto):
        print('Salário Bruto: (', valor, '*', horas, '): R$',
              salario, '\n(-) IR (', irPercentual, '% ): R$', ir,
              '\n(-) INSS (10 %): R$', (salario * 0.1),
              '\nFGTS (11 % ): R$ ', (salario * 0.11),
              '\nTotal de descontos: R$', (salario * 0.1) + ir)
    else:
        print('Salário Bruto: (', valor, '*', horas, '): R$',
              '\n(-) INSS (10 %): R$', (salario * 0.1),
              '\nFGTS (11 % ): R$ ', (salario * 0.11),
              '\nTotal de descontos: R$', (salario * 0.1))


def main():
    valorHora = float(input('valor da hora: '))
    horasMes = int(input('Horas trabalhadas no Mês: '))

    calculo(valorHora, horasMes)


main()