def calculo(tamanho, velocidade):
    valor = tamanho / velocidade
    return valor * 60


def main():
    tamanho = int(input('Tamanho do arquivo em MB: '))
    velocidade = int(input('Velocidade da Internet: '))

    print('O download será concluído em ',
          calculo(tamanho, velocidade), 'minutos')


main()


