def produto(valor1, valor2):
    return ((valor1 * 2) * valor2/2)


def soma(valor1, valor3):
    return ((valor1*3) + valor3)


def cubo(valor3):
    return pow(valor3, 3)


def main():
    numero1 = int(input('Primeiro inteiro: '))
    numero2 = int(input('Segundo inteiro: '))
    numero3 = float(input('Número real: '))

    print('O produto do dobro do primeiro com metade do segundo: ',
          produto(numero1, numero2))
    print('A soma do triplo do primeiro com o terceiro: ', soma(numero1, numero3))
    print('O terceiro elevado ao cubo: ', cubo(numero3))


main()
