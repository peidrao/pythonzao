def areaOfCube(value):
    return value*value


lado = float(input('Digite o lado do quadrado: '))

print('Área do quadrado: ', areaOfCube(lado), ' cm²')
print('O  dobro da área do quadrado: ',
      areaOfCube(lado) * areaOfCube(lado), ' cm²')

