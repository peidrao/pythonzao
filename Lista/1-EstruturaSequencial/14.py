def calculo(valor):
    cobertura = valor / 3
    latas = cobertura / 18
    latas_int = int(latas)
    valorTotal = latas_int * 80
    print('\n\nTotal de latas:', latas_int,
          '\nValor Total total: R$', valorTotal)


def main():
    tamanho = float(input('Tamanho em metros da área a ser pintada: '))
    calculo(tamanho)


main()
