def calcularPesoIdeal(altura, sexo):
    if sexo == 'h':
        return (72.7 * altura) - 58
    else:
        return (62.1*altura)-44.7


def main():
    sexo = input('Qual seu sexo (h-homem/m-mulher): ')
    altura = float(input('Sua altura: '))

    print('Seu peso ideal é: ', calcularPesoIdeal(altura, sexo))


main()
