def ctoF(temperature):
    return ((temperature * 9/5) + 32)


def main():
    temperatura = float(input('Temperatura em Celsius: '))
    print('Temperatura em Fahrenheit: ', ctoF(temperatura))


main()
