def calculo(salario, horas):
    total = salario * horas
    ir = total * 0.11
    inss = total * 0.08
    sindicato = total * 0.08
    liquido = total - (ir+inss+sindicato)
    print('\n - Salário Bruto : R$', total,
          '\n* IR (11%) : R$', ir, '\n* INSS (8%) : R$', inss, '\n* Sindicato (5%) : R$', sindicato, '\n= Salário Líquidp : R$', liquido)


def main():
    salario = float(input('Quanto ganha por hora: '))
    horas = int(input('Horas trabalhadas no mês: '))

    calculo(salario, horas)


main()
