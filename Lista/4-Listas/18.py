flag = False
sistemas = [0, 0, 0, 0, 0, 0]


while flag != True:
    print('1- Windows Server'
          '\n2- Unix'
          '\n3- Linux'
          '\n4- Netware'
          '\n5- Mac OS'
          '\n6- Outro')

    opc = int(input('Escolha o melhor sistema: '))
    if(opc >= 1 and opc <= 6):
        sistemas[opc - 1] += 1
    elif(opc == 0):
        break

print('Sistema Operacional     Votos   %'
      '\n-------------------     -----   ---')
print('Windows Server\t\t', sistemas[0], '    ',
      int(sistemas[0] / sum(sistemas) * 100), '%')

print('Unix\t\t\t', sistemas[1], '    ',
      int(sistemas[1] / sum(sistemas) * 100), '%')

print('Linux\t\t\t',  sistemas[2], '    ',
      int(sistemas[2] / sum(sistemas) * 100), '%')

print('Netware\t\t\t',  sistemas[3], '    ',
      int(sistemas[3] / sum(sistemas) * 100), '%')

print('Mac OS\t\t\t',  sistemas[4], '    ',
      int(sistemas[4] / sum(sistemas) * 100), '%')

print('Outro\t\t\t',  sistemas[5], '    ',
      int(sistemas[5] / sum(sistemas) * 100), '%')

print('-------------------     -----'
      '\nTotal\t\t\t', sum(sistemas))


""" print(nomesSistemas[i], '\t\t', , '\t\t',
      float((sistemas[i] / sum(sistemas[i])) * 100), '%') """
