jogadores = []
flag = False
flag2 = False
somatorio = 0

for i in range(1, 24):
	jogadores.append(0) 

while flag != True:
	nota = int(input('Nota para qual jogador: ')) 
	if(nota == 0):
		break;
	elif(nota < 0 or nota > 23):
		print('Não existe esse jogador!\n')
	elif(nota >= 1 or nota <=23): 
		somatorio += 1 
		jogadores[nota - 1] += 1
				
		

print('Programa encerrado!\n\n')

print('Total de votos computados: ', somatorio)

for i in range(0,23):
	if(jogadores[i] > 0):
		print('Jogador', i+1, ': ', jogadores[i], 'votos | Porcentagem: ', round((jogadores[i] / somatorio) * 100), '%')
		

indice = 0
for i in range(0, 23):
	if jogadores[i] > indice:
		indice = i
		partidas = jogadores[i]
		porcentagem = round((jogadores[i] / somatorio) * 100)

print('\nMelhor Jogador: ', indice + 1, '| Número de partidas: ', partidas, ' | Porcentagem: ', porcentagem, '%')
