perguntas = []

perguntas.append(input('Telefonou para a vítima? (s/n): '))
perguntas.append(input('Esteve no local do crime? (s/n): '))
perguntas.append(input('Mora perto da vítima? (s/n): '))
perguntas.append(input('Devia para a vítima? (s/n): '))
perguntas.append(input('Já trabalhou com a vítima? (s/n): '))

cont = 0

for i in range(0, 5):
    if(perguntas[i] == 's'):
        cont += 1
if(cont == 2):
    print('Suspeito(a)')
elif(cont >= 3 and cont <= 4):
    print('Cúmplice')
elif(cont == 5):
    print('Assassino(a)')
else:
    print('Inocente!')
