mes = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
       'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']

temperaturas = []
for i in range(12):
    print('Temperatura de ', mes[i], end=": ")
    temperatura = float(input())
    temperaturas.append(temperatura)

media = sum(temperaturas) / 12

print('\n\nTemperaturas que passaram da média de', media)
for i in range(0, 12):
    if(temperaturas[i] > media):
        print(mes[i], 'temperatura: ', temperaturas[i])
