saltos = []
flag = False

while flag != True:
	nome = input('Seu nome: ')
	if(len(nome) == 0):
		flag = True
	else: 
		for i in range(5):
			print('Salto (', i+1, '):', end=" ")
			saltos.append(float(input()))
		
		print('\nAtleta: ', nome)
		print('Primeiro salto: ', saltos[0], 'm')
		print('Segundo salto: ', saltos[1], 'm')
		print('Terceiro salto: ', saltos[2], 'm')
		print('Quarto salto: ', saltos[3], 'm')
		print('Quinto salto: ', saltos[4], 'm')		

		print('\nResultado final')
		print('Atleta: ', nome)
		print('Saltos:', saltos)
		print('Média dos saltos: ', round((sum(saltos) / len(saltos)), 2) )
		print('\n\n')	

