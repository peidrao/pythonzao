numeros = []

for i in range(10):
    numero = int(input('Número: '))
    quadrado = pow(numero, 2)
    numeros.append(quadrado)

print('Soma de todos os quadrados: ', sum(numeros))
