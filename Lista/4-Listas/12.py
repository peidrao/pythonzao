idades = []
alturas = []
total = 0.0

for i in range(3):
    idades.append(int(input('Idade: ')))
    altura = float(input('Altura: '))
    alturas.append(altura)
    total += altura


media = total / len(alturas)

cont = 0
for i in range(0, len(idades)):
    if idades[i] > 13:
        if alturas[i] < media:
            cont += 1


print(cont, 'Alunos tem mais de 13 anos e possuem altura inferior a:', round(media, 2))
