def eleicao():
    voto = int
    candidato1 = candidato2 = candidato3 = candidato4 = votoNulo = votoBranco = total = 0
    while voto != 0:
        print('\n1- José'
              '\n2- João'
              '\n3- Alberto'
              '\n4- Marcos'
              '\n5- Voto nulo'
              '\n6- Voto em branco')

        voto = int(input('\nDigite seu voto: '))
        total += 1
        if voto == 1: candidato1 += 1
        elif voto == 2: candidato2 += 1
        elif voto == 3: candidato3 += 1
        elif voto == 4: candidato4 += 1
        elif voto == 5: votoNulo += 1
        elif voto == 6: votoBranco += 1

    print('\nJosé: ', candidato1, '\nJoão:', candidato2, '\nAlberto:',
          candidato3, '\nMarcos:', candidato4)
    print('Total de votos nulos', votoNulo)
    print('Total de votos brancos', votoBranco)
    print('Porcentagem de votos nulos: ', round((votoNulo / total * 100), 2))
    print('Porcentagem de votos brancos: ', round((votoBranco / total * 100),
                                                  2))


eleicao()