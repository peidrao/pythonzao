def calculo():
    salario = 1000
    aumento = 15/100
    ano = 1997
    novo_salario = salario * (1 + aumento)

    while ano <= 2020:
        aumento = aumento * 2
        novo_salario = novo_salario * (1 + aumento)
        ano += 1

    print('Salario atual', novo_salario)


calculo()
