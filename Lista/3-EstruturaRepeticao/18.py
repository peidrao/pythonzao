def main():
    lista = []
    n = int(input('Digite a quantidade de números: '))
    soma = 0

    for i in range(n):
        numero = float(input('Digite o valor: '))
        while numero > 1000 or numero < 0:
            numero = float(input('Digite o valor entre 0 e 1000: '))

        soma += numero
        lista.append(numero)

    print('Soma de todos os números: ', soma, '\nMaior número: ',
          max(lista), '\nMenor número: ', min(lista))


main()
