def gabarito():
    continuar = False
    alunos = []
    total = 0
    respostas = [10]
    gabarito = [
        'A',
        'B'
        'C',
        'D',
        'E',
        'E',
        'D',
        'C',
        'B',
        'A',
    ]
    while continuar != True:
        total += 1
        cont = 0

        for i in range(10):
            print(i + 1, end=" ")
            respostas.extend(input(''))

        for respostas, gabarito in zip(respostas, gabarito):
            if respostas == gabarito:
                cont += 1

        alunos.append(cont)
        print('total: ', cont)
        cont = 0

        opc = input('Deseja continuar: (s/n): ')
        if (opc == 'n'): continuar = True

    print('Maior número de acertos: ', max(alunos))
    print('Menor número de acertos: ', min(alunos))
    print('Total de alunos que usaram o sistema: ', total)
    print('Média da turma: ', (sum(alunos) / total))


gabarito()