def main():
    n = int(input('Digite um número: '))
    max = 0
    min = 10000
    total = 0
    for i in range(n):
        valor = int(input('Valor: '))
        total += valor
        if valor > max:
            max = valor
        if valor < min:
            min = valor

    print('Valor máximo:', max, '\nValor mínimo:', min, '\nTotal:', total)


main()
