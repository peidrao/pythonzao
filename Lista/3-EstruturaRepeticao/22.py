def mediaN():
    n = int(input('Quantas notas deseja calcular: '))
    soma = 0
    for i in range(n):
        nota = float(input('Nota : '))
        soma += nota

    media = soma / n

    if(0 <= media <= 25):
        print('Jovem')
    elif(25 < media <= 60):
        print('Adulto')
    elif (media > 60):
        print('Idoso')


mediaN()
