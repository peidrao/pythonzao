def turmas():
    turmas = int(input('Quantidade de turmas: '))
    contador = 0
    soma = 0
    while contador != turmas:
        alunos = int(input('Alunos da turma: '))
        while alunos > 40:
            alunos = int(input('Alunos da turma (menos de 40): '))
        soma += alunos
        contador += 1

    print('A média de alunos por turma é ', soma / turmas)


turmas()
