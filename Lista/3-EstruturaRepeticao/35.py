def alunos():
    alunos = []
    alturas = []

    for i in range(5):
        alunos.append(int(input('Número do aluno: ')))
        alturas.append(float(input('Altura d aluno: ')))

    maisAlto = alturas.index(max(alturas))
    maisBaixo = alturas.index(min(alturas))

    print('Aluno mais alto: ', alunos[maisAlto], "Altura: ", max(alturas))
    print('Aluno mais baixo: ', alunos[maisBaixo], "Altura: ", min(alturas))

alunos()