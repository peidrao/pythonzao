def main():
    contadorPar = 0
    contadorImpar = 0
    for i in range(10):
        numero = int(input('Digite um número: '))
        if numero % 2 == 0:
            contadorPar += 1
        else:
            contadorImpar += 1

    print('O número de pares é: ', contadorPar,
          '\nO número de ímpares é:', contadorImpar)


main()
