def calculo(cidadeA, cidadeB, taxaA, taxaB):
    contador = 0
    flag1 = False
    flag2 = False
    if(cidadeA < cidadeB):
        while(cidadeB > cidadeA):
            cidadeA += (cidadeA * taxaA)
            cidadeB += (cidadeB * taxaB)
            contador += 1
        flag1 = True
    elif(cidadeA > cidadeB):
        while(cidadeB < cidadeA):
            cidadeA += (cidadeA * taxaA)
            cidadeB += (cidadeB * taxaB)
            contador += 1
        flag2 = True

    if(flag1 == True):
        print("Cidade A demorou", contador,
              'anos para passar a população da cidade B')
    elif(flag2 == True):
        print("Cidade B demorou", contador,
              'anos para passar a população da cidade A')


def main():
    flag = False
    while(flag != True):
        A = int(input('População da cidade A: '))
        B = int(input('População da cidade B: '))

        taxaA = float(input('Digite a taxa de crescimento da cidade A: '))
        taxaB = float(input('Digite a taxa de crescimento da cidade B: '))

        calculo(A, B, taxaA / 100, taxaB / 100)

        opcao = input('Deseja fazer mais entradas? (s/n): ')
        if(opcao == 's'):
            flag = False
        else:
            flag = True


""" Altere o programa anterior permitindo ao usuário informar as 
populações e as taxas de crescimento iniciais. Valide a entrada e permita repetir a operação. """

main()
