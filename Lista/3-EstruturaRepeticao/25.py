def main():
    quantidade = int(input('Quantidade de CDs: '))
    contador = 0
    total = 0

    while contador != quantidade:
        cd = float(input('Valor do CD: '))
        total += cd
        contador += 1

    print('Quantidade de CDs: ', quantidade, '\nValor total: R$',
          round(total, 2), '\nValor médio por CD: R$', round((total / quantidade), 2))


main()
