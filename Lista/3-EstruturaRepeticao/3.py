def main():
    contador = 0
    _nome = False
    _idade = False
    _salario = False
    _sexo = False
    _estadoCivil = False

    while(contador < 5):
        if(_nome == False):
            nome = input('Qual seu nome: ')
            if(len(nome) > 3):
                _nome = True
                contador += 1
        if(_idade == False):
            idade = int(input('Qual sua idade: '))
            if(0 < idade < 150):
                _idade = True
                contador += 1
        if(_salario == False):
            salario = float(input('Qual seu salário: '))
            if(salario > 0):
                _salario = True
                contador += 1
        if(_sexo == False):
            sexo = input('Qual seu sexo: ')
            if(sexo == 'f' or sexo == 'm'):
                _sexo = True
                contador += 1
        if(_estadoCivil == False):
            estadoCivil = input(
                'Estado Civil (s-solteiro;c-casado;v-viuvo;d-divorciado): ')
            if(estadoCivil == 's' or estadoCivil == 'c' or estadoCivil == 'v' or estadoCivil == 'd'):
                _estadoCivil = True
                contador += 1

    print(contador)


main()
