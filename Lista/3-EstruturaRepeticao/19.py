def fatorialV2():
    flag = False

    while flag != True:
        numero = int(input('Número: '))

        while numero < 0 or numero >= 17:
            numero = int(input('Número: '))
        fat = numero
        total = numero
        while numero != 1:
            total *= (numero - 1)
            numero -= 1

        print(fat, '! = ', total)

        opc = input('\n\nDeseja Continuar:  s-sim / n-nao: ')
        if(opc == 's'):
            flag = False
        else:
            flag = True


fatorialV2()
