def tabuada():
    tabuada = int(input('Qual tabuada deseja calcular: '))
    primeiro = int(input('Primeiro elemento a ser calculado: '))
    segundo = int(input('Segundo elemento a ser calculado: '))

    for i in range(primeiro, segundo+1):
        print(tabuada, 'x', i, '=', tabuada*i)


tabuada()
