def academia():
    codigo = []
    altura = []
    peso = []
    contador = 0

    while codigo != 0:
        codigo.append(int(input('Código: ')))
        if codigo[contador] == 0:
            break
        altura.append(float(input(('Altura: '))))
        peso.append(float(input(('Peso: '))))
        contador += 1

    indexAltura = altura.index(max(altura))
    indexPeso = peso.index(max(peso))

    print('\nMaior altura:', max(altura), '| Código: ', codigo[indexAltura])
    print('Maior Peso:', max(peso), '| Código: ', codigo[indexPeso])
    print('Média das alturas:', round((sum(altura) / contador), 2))
    print('Média dos pesos:', round((sum(peso) / contador), 2))


academia()
